This module contains basic integration with Interkassa 2.0.
https://interkassa.com/

The module works with Drupal Commerce (https://drupal.org/project/commerce).

INSTALLATION:
Before you'll start the installation process you must register on Interkassa
https://interkassa.com/ and create your own Checkout.

1) Download the module from Drupal.org and extract it to your modules folder.
2) Enable it.
3) Go to /admin/commerce/config/payment-methods and edit Interkassa rule.
4) Edit Payment method
(Just click edit before 'Enable payment method: Interkassa payment').
5) Setup the settings according your data from Interkassa.

That's it :)

Additional information:
Default Success url - /commerce_interkassa/commerce_interkassa/status
Default Fail url - /commerce_interkassa/commerce_interkassa/status
Default Pending url - /commerce_interkassa/commerce_interkassa/status
Default Interaction url - /commerce_interkassa/commerce_interkassa/interaction
