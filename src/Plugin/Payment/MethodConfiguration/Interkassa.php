<?php

namespace Drupal\interkassa_payment\Plugin\Payment\MethodConfiguration;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\payment_offsite_api\Plugin\Payment\MethodConfiguration\PaymentMethodConfigurationBaseOffsite;

/**
 * Provides the configuration for the payment_interkassa payment method plugin.
 *
 * Plugins extending this class should provide a configuration schema that
 * extends
 * plugin.plugin_configuration.payment_method_configuration.payment_interkassa.
 *
 * @PaymentMethodConfiguration(
 *   id = "payment_interkassa",
 *   label = @Translation("Interkassa"),
 *   description = @Translation("A payment method type that process payments via Interkassa payment gateway.")
 * )
 */
class Interkassa extends PaymentMethodConfigurationBaseOffsite implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'message_text' => 'In addition to the order amount interkassa fee can be charged.',
      'message_text_format' => 'plain_text',
      'ipn_statuses' => [
        'new' => 'payment_created',
        'wait_accept' => 'payment_pending',
        'success' => 'payment_success',
        'process' => 'payment_pending',
        'canceled' => 'payment_cancelled',
        'fail' => 'payment_failed',
      ],
      'config' => [
        'ik_co_id' => '',
        'action_url' => 'https://sci.interkassa.com',
        'ik_cur' => [],
        'ik_cur_default' => '',
        'sign_key' => '',
        'test_key' => '',
        'hash_type' => 'md5',
      ],
    ];
  }

  /**
   * Returns the hash method to sign exchange data.
   *
   * @return string
   *   The hash method.
   */
  public function getHashType() {
    return $this->configuration['config']['hash_type'];
  }

  /**
   * {@inheritdoc}
   */
  public function processBuildConfigurationForm(array &$element, FormStateInterface $form_state, array &$form) {
    $element['ik_co_id'] = [
      '#type' => 'textfield',
      '#title' => t('Checkout ID'),
      '#default_value' => $this->getCheckoutId(),
      '#maxlength' => 256,
      '#required' => TRUE,
    ];

    $element['ik_cur'] = [
      '#type' => 'checkboxes',
      '#title' => t('Currencies'),
      '#default_value' => $this->getCurrencies(),
      '#options' => $this->getSupportedCurrencies(),
      '#required' => TRUE,
    ];

    $element['ik_cur_default'] = [
      '#type' => 'select',
      '#title' => t('Default currency'),
      '#default_value' => $this->getDefaultCurrency(),
      '#options' => $this->getSupportedCurrencies(),
      '#required' => TRUE,
    ];

    $element['action_url'] = [
      '#type' => 'textfield',
      '#title' => t('Action URL'),
      '#default_value' => $this->getActionUrl(),
      '#description' => $this->t('Default action url :action_url', [':action_url' => 'https://sci.interkassa.com']),
      '#maxlength' => 256,
      '#required' => TRUE,
    ];

    $element['sign_key'] = [
      '#type' => 'textfield',
      '#title' => t('Sign Key'),
      '#default_value' => $this->getSignKey(),
      '#maxlength' => 256,
      '#required' => TRUE,
    ];

    $element['test_key'] = [
      '#type' => 'textfield',
      '#title' => t('Test Key'),
      '#default_value' => $this->getSignKey(TRUE),
      '#maxlength' => 256,
      '#required' => TRUE,
    ];

    return parent::processBuildConfigurationForm($element, $form_state, $form);
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
//    if (valid_url($settings['action_url'])) {
//      form_set_error(t('Action URL'), t('You must enter a valid url.'));
//    }
    parent::validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $parents = $form['plugin_form']['#parents'];
    $values = $form_state->getValues();
    $values = NestedArray::getValue($values, $parents);

    $this->setCheckoutId($values['ik_co_id']);
    $this->setCurrencies($values['ik_cur']);
    $this->setDefaultCurrency($values['ik_cur_default']);
    $this->setActionUrl($values['action_url']);
    $this->setSignKey($values['sign_key']);
    $this->setSignKey($values['test_key'], TRUE);
  }

  /**
   * Returns the CheckoutId.
   *
   * @return string
   */
  public function getCheckoutId() {
    return $this->configuration['config']['ik_co_id'];
  }

  /**
   * Sets the CheckoutId.
   *
   * @param string $ik_co_id
   *   The checkout merchant ID.
   *
   * @return static
   */
  public function setCheckoutId($ik_co_id) {
    $this->configuration['config']['ik_co_id'] = $ik_co_id;

    return $this;
  }

  /**
   * Returns list of configured currencies.
   *
   * @return array
   */
  public function getCurrencies() {
    return $this->configuration['config']['ik_cur'];
  }

  /**
   * Returns supported currency list.
   *
   * @return array
   *   Keyed array of currencies.
   */
  public function getSupportedCurrencies() {
    return [
      'USD' => t('USD'),
      'EUR' => t('EUR'),
      'UAH' => t('UAH'),
      'RUB' => t('RUB'),
    ];
  }

  /**
   * Sets the enabled currencies.
   *
   * @param array $ik_cur
   *   An array of currencies.
   *
   * @return static
   */
  public function setCurrencies($ik_cur) {
    $this->configuration['config']['ik_cur'] = $ik_cur;

    return $this;
  }

  /**
   * Returns the default configured currency.
   *
   * @return string
   *   The currency code.
   */
  public function getDefaultCurrency() {
    return $this->configuration['config']['ik_cur_default'];
  }

  /**
   * Sets the default currency.
   *
   * @param string $ik_cur_default
   *   The currency code.
   *
   * @return static
   */
  public function setDefaultCurrency($ik_cur_default) {
    $this->configuration['config']['ik_cur_default'] = $ik_cur_default;

    return $this;
  }

  /**
   * Returns the gateway action URL.
   *
   * @return string
   *   The gateway URL.
   */
  public function getActionUrl() {
    return $this->configuration['config']['action_url'];
  }

  /**
   * Sets the gateway URL.
   *
   * @param string $action_url
   *   The gateway URL.
   *
   * @return static
   */
  public function setActionUrl($action_url) {
    $this->configuration['config']['action_url'] = $action_url;

    return $this;
  }

  /**
   * Returns the configured signature key.
   *
   * @param bool $test
   *   (Optional) TRUE to return demo signature.
   *
   * @return string
   *   The signature key.
   */
  public function getSignKey($test = FALSE) {
    $key_type = $test ? 'test_key' : 'sign_key';
    return $this->configuration['config'][$key_type];
  }

  /**
   * Sets the server signature key.
   *
   * @param string $key
   *   The new key.
   * @param bool $test
   *   (Optional) TRUE to set demo key.
   *
   * @return static
   */
  public function setSignKey($key, $test = FALSE) {
    $key_type = $test ? 'test_key' : 'sign_key';
    $this->configuration['config'][$key_type] = $key;

    return $this;
  }

}
